# Homework due on Tuesday
# Look for code to be posted
# Double underscored functions are called "magic" functions

# height = 0
# width = 0
# def area():
# 	return height * width / 2

# height = 1
# width = 0.5
# print "The area is", area()


# or with Object Oriented Programming
class Triangle:
	def __init__(self):
		self.height = 0
		self.width = 0
		self.area = self.getArea()
	def getArea(self):
		return self.height * self.width / 2

tr = Triangle()

print tr.height

tr.height = 2.0
tr.width = 3.5

print tr.getArea()

import math
import numpy

# now do the same thing with a circle
class Circle:
	def __init__(self):
		self.radius = 0
		self.diameter = self.radius * 2
		self.area = self.getArea()
	def getArea(self):
		return self.radius**2 * math.pi

cir = Circle()

print cir.radius

cir.radius = 1
print "Area is", cir.getArea()